/// <reference type="cypress />"
// .md 2021:03:07
// Mooncascade QA homework

describe('Add new user and logout fuction', function(){
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })
    // goto page
    it('Verify page title',function(){
        cy.visit('https://www.gymwolf.com/staging/')
		// verify header
        cy.title().should('eq','Gymwolf - Jõusaali päevik, jälgi oma jõusaalitreeninguid Internetis')
    })
	// register email
	it('Register new user',function(){
        cy.get('[class="show-signup-form-from-menu"]').click()

		cy.get('[name="signup_email"]').eq(0).type('testtest@mooncascade.com')
		cy.get('[type="submit"]').contains('Registreeru').click()
		// fill in form
		cy.get('[id=gwn]').type('MC Tester')
		cy.get('[id=gender_male]').click()
		// birth date
		cy.get('[name="Date_Month"]').select('February').should('have.value', '02')
		cy.get('[name="Date_Month"]').select('April').should('have.value', '04')
		cy.get('[name="Date_Day"]').select('20').should('have.value', '20')
		cy.get('[name="Date_Year"]').select('2000').should('have.value', '2000')
		// password
		cy.get('[id="gwp"]').type('lollakas')
		cy.get('[id="gwp2"]').type('lollakas')
		cy.get('.btn-success').eq(0).click()
        cy.wait(500)
    })

	// logout
	it('Logout',function(){
       cy.get('[class="menu-name-label"]').contains('MC Tester').click()
	   cy.get('a[href="/staging/logout"').eq(1).click()
    })
	
	// login
	it('Login',function(){
	   cy.get('a').contains('Logi sisse').should('have.attr', 'href', '#login-front').click()
	   cy.wait(500)
	   cy.get('[placeholder="E-mail"]').eq(0).click().type('testtest@mooncascade.com')
	   cy.get('[placeholder="Parool"]').eq(0).type('lollakas').type('{enter}')
    })
})


